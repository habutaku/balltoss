﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusketGenerator : MonoBehaviour
{
    public GameObject BasketPrefab;
    float span = 1.8f;
    float delta = 0;

    // Update is called once per frame
    void Update()
    {
        //1秒間隔にかごを生成する
        this.delta += Time.deltaTime;
        if(this.delta>this.span)
        {
            //BasketPrefabに関連付けさせる
            this.delta = 0;
            GameObject go = Instantiate(BasketPrefab) as GameObject;
            
        }
    }
}
