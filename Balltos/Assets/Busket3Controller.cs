﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Busket3Controller : MonoBehaviour
{
    GameObject Arrow;
    GameObject BallPrefab;
    float px = 0;


    // Start is called before the first frame update
    void Start()
    {
        //BallPrefabを探す
        this.BallPrefab = GameObject.Find("BallPrefab");

        //座標をランダムに設定する
        px = Random.Range(0.1f, 0.2f);

    }

    // Update is called once per frame
    void Update()
    {
        //かごの位置を決める
        transform.Translate(px, 0, 0);

        //x軸が10以上になったら消す
        if (transform.position.x > 10.0f)
        {
            Destroy(gameObject);
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Ball")
        {
            //かごに当たった時に玉を消す

            Debug.Log("ゴール");
            Destroy(other.gameObject);
            //GameDirectorのDecreaseCoを呼び出す

            GameObject Director = GameObject.Find("GameDirector");
            Director.GetComponent<GameDirector>().DecreaseCo3();
        }
        

    }
}
