﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameDirector : MonoBehaviour
{
    //ほかのスクリプトにも移せる変数

    public static int Co = 0;
    public static int Co2 = 0;
    public static float time = 1800.0f;

    public GameObject BallCount = null;
    public GameObject BallCount2 = null;
    GameObject timer;
   
    // Start is called before the first frame update
    void Start()
    {
        //timerを探す
        this.timer = GameObject.Find("timer");

        //時間とスコアの初期数字を設定
        time = 30.0f;
        Co = 0;
        Co2 = 0;
    }

    public void DecreaseCo()
    {
        //スコアを１ずつ増やす
        Co++;
    }
    public void DecreaseCo2()
    {
        //スコアを１ずつ増やす
        Co2++;
    }
    public void DecreaseCo3()
    {
        Co += 3;
    }
    public void DecreaseCo4()
    {
        Co2 += 3;
    }
    // Update is called once per frame
    void Update()
    {
        //スコアを表示する
        Text arrowText = this.BallCount.GetComponent<Text>();
        arrowText.text = Co + "個";

        Text arrowText2 = this.BallCount2.GetComponent<Text>();
        arrowText2.text = Co2 + "個";

        //時間が減っていく
        time -= Time.deltaTime;
        this.timer.GetComponent<Image>().fillAmount -= 0.00056f;

        if (time <= 0)
        {
            if (Co > Co2)
            {
                SceneManager.LoadScene("clearScene");

            }
            else if (Co < Co2)
            {
                SceneManager.LoadScene("BlueclearScene");

            }
        }
    }
}
