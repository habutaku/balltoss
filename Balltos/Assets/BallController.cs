﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{

    Rigidbody2D rigid2D;
    float BoundForce = 280.0f;



    // Start is called before the first frame update
    void Start()
    {
        //上方向のベクトルをかけてオブジェクトを動かす
        this.rigid2D = GetComponent<Rigidbody2D>();
        this.rigid2D.AddForce(transform.up * this.BoundForce);

    }

    // Update is c0;0alled once per frame
    void Update()
    {
        //y軸が-10より少なければオブジェクトを消す

        if (transform.position.y < -10.0f)
        {
            Destroy(gameObject);
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //かごに当たった時に玉を消す

        Debug.Log("ゴール");
        Destroy(gameObject);
        //GameDirectorのDecreaseCoを呼び出す

        GameObject Director = GameObject.Find("GameDirector");
        Director.GetComponent<GameDirector>().DecreaseCo();


    }
}
