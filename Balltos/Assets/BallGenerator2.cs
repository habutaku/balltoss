﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator2 : MonoBehaviour
{
    public GameObject BallPrefab2;
    float span = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //GameDirectorの中にあるtimeを呼び出す

        float time = GameDirector.time;
        span -= Time.deltaTime;

        //時間が0より多い間は玉をだす
        if (time > 0)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (span < 0)
                {
                    GameObject ball2 = Instantiate(BallPrefab2) as GameObject;
                    ball2.transform.position = transform.position;
                    ball2.transform.rotation = transform.rotation;
                    span = 0.4f;
                }
            }
        }
    }
}
