﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour
{
    public GameObject BallPrefab;
    float span = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //GameDirectorの中にあるtimeを呼び出す

        float time = GameDirector.time;
        span -= Time.deltaTime;

        //時間が0より多い間は玉をだす
        if (time > 0)
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                if (span < 0)
                {
                    GameObject ball = Instantiate(BallPrefab) as GameObject;
                    ball.transform.position = transform.position;
                    ball.transform.rotation = transform.rotation;
                    span = 0.4f;
                }
            }
        }
    }
}
