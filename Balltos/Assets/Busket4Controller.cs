﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Busket4Controller : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Ball")
        {
            //かごに当たった時に玉を消す

            Debug.Log("ゴール");
            Destroy(other.gameObject);
            //GameDirectorのDecreaseCoを呼び出す

            GameObject Director = GameObject.Find("GameDirector");
            Director.GetComponent<GameDirector>().DecreaseCo4();
        }

    }
}
